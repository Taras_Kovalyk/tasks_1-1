﻿using System;

namespace Task_2
{    
    class Rectangle
    {
        public double HorizontalLength { get; private set; }
        public double VerticalLength { get; private set; }

        private Point downLeftPoint;

        protected Rectangle() { }

        public Rectangle(double horizontalLength, double verticalLength, Point downLeftPoint)
        {
            this.downLeftPoint = new Point(downLeftPoint.X, downLeftPoint.Y);
            HorizontalLength = horizontalLength;
            VerticalLength = verticalLength;
        }

        public void Move(double verticalStep, double horizontalStep)
        {
            downLeftPoint.X += horizontalStep;
            downLeftPoint.Y += verticalStep;

        }

        public void ChangeVerticalSize(double length)
        {
            VerticalLength += length;
        }
        public void ChangeHorizontalSize(double length)
        {
            HorizontalLength += length;
        }
        public void Rescale(double scale)
        {
            VerticalLength += scale;
            HorizontalLength += scale;
        }

        public static Rectangle FindIntersection(Rectangle firstRect, Rectangle secondRect)
        {
            Rectangle rightRect, leftRect;
            Rectangle upperRect, lowerRect;
            Rectangle intersectRect = new Rectangle();

            if (firstRect.GetDownLeftX() > secondRect.GetDownLeftX())
            {
                rightRect = firstRect;
                leftRect = secondRect;
            }
            else
            {
                rightRect = secondRect;
                leftRect = firstRect;
            }

            if (firstRect.GetDownLeftY() > secondRect.GetDownLeftY())
            {
                upperRect = firstRect;
                lowerRect = secondRect;
            }
            else
            {
                upperRect = secondRect;
                lowerRect = firstRect;
            }


            // if X coordinate of right rectangle is in the gap between X coordinates of left rectangle
            if (rightRect.GetDownLeftX() <= leftRect.GetDownLeftX() + leftRect.HorizontalLength
                && rightRect.GetDownLeftX() >= leftRect.GetDownLeftX())
            {
                intersectRect.SetDownLeftX(rightRect.GetDownLeftX());
                intersectRect.HorizontalLength = leftRect.HorizontalLength - rightRect.GetDownLeftX() + leftRect.GetDownLeftX();
            }
            else
            {
                // if not intersect
                return null;
            }

            // if Y coordinate of upper rectangle is in the gap between Y coordinates of lower rectangle
            if (upperRect.GetDownLeftY() <= lowerRect.GetDownLeftY() + lowerRect.VerticalLength
                && upperRect.GetDownLeftY() >= lowerRect.GetDownLeftY())
            {
                intersectRect.SetDownLeftY(upperRect.GetDownLeftY());
                intersectRect.VerticalLength = lowerRect.VerticalLength - upperRect.GetDownLeftY() + lowerRect.GetDownLeftY();
            }
            else
            {
                return null;
            }

            return intersectRect;
        }

        public static Rectangle FindUnion(Rectangle firstRect, Rectangle secondRect)
        {
            Rectangle rightRect, leftRect;
            Rectangle upperRect, lowerRect;
            Rectangle unionRect = new Rectangle();

            if (firstRect.GetDownLeftX() > secondRect.GetDownLeftX())
            {
                rightRect = firstRect;
                leftRect = secondRect;
            }
            else
            {
                rightRect = secondRect;
                leftRect = firstRect;
            }

            if (firstRect.GetDownLeftY() > secondRect.GetDownLeftY())
            {
                upperRect = firstRect;
                lowerRect = secondRect;
            }
            else
            {
                upperRect = secondRect;
                lowerRect = firstRect;
            }

            // if X coordinate of right rectangle is in the gap between X coordinates of left rectangle
            if (rightRect.GetDownLeftX() <= leftRect.GetDownLeftX() + leftRect.HorizontalLength
                && rightRect.GetDownLeftX() >= leftRect.GetDownLeftX()
                && upperRect.GetDownLeftY() <= lowerRect.GetDownLeftY() + lowerRect.VerticalLength
                && upperRect.GetDownLeftY() >= lowerRect.GetDownLeftY())
            {
                unionRect.SetDownLeftX(leftRect.GetDownLeftX());
                unionRect.HorizontalLength = rightRect.GetDownLeftX() - leftRect.GetDownLeftX() + rightRect.HorizontalLength;
                unionRect.SetDownLeftY(lowerRect.GetDownLeftY());
                unionRect.VerticalLength = upperRect.VerticalLength + upperRect.GetDownLeftY() - lowerRect.GetDownLeftY();
            }
            else
            {
                // if not intersect
                unionRect.SetDownLeftX(lowerRect.GetDownLeftX());
                unionRect.SetDownLeftY(lowerRect.GetDownLeftY());
                unionRect.VerticalLength = upperRect.GetDownLeftY() - lowerRect.GetDownLeftY() + upperRect.VerticalLength;
                unionRect.HorizontalLength = rightRect.GetDownLeftX() - leftRect.GetDownLeftX() + rightRect.HorizontalLength;
            }

            return unionRect;
        }

        public void ShowRectangle()
        {
            Console.WriteLine("\n\tДовжина: {0}", HorizontalLength);
            Console.WriteLine("\n\tВисота: {0}", VerticalLength);
            Console.WriteLine("\n\tЛiва нижня точка: {0}", downLeftPoint.ToString());
        }

        public double GetDownLeftX()
        {
            return downLeftPoint.X;
        }

        public double GetDownLeftY()
        {
            return downLeftPoint.Y;
        }

        public void SetDownLeftX(double x)
        {
            downLeftPoint.X = x;
        }

        public void SetDownLeftY(double y)
        {
            downLeftPoint.Y = y;
        }

    }
}
