﻿
namespace Task_2
{
    struct Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            this.X = x; this.Y = y;
        }

        public override string ToString()
        {
            return "\tPoint: " + "(" + X.ToString() + "," + Y.ToString() + ")" + ".";
        }
    }
}
