﻿using System;

namespace Task_4
{
    class Matrix
    {
        double[,] matrix;
        static Random rand = new Random();

        protected Matrix() { }

        public Matrix(int rows, int cols)
        {
            matrix = new double[rows, cols];
        }

        public void FillRandom()
        {
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(1,10);
                }
            }
        }

        public void ShowMatrix()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(string.Format("{0} ", matrix[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);
        }

        public double this[int r, int c]
        {
            get { return matrix[r, c]; }
            set { matrix[r, c] = value; }
        }

        public int GetLength(int dimension)
        {
            return matrix.GetLength(dimension);
        }

        public double Determinant()
        {
            return MatrixDeterminant(this.matrix);
        }

        public double MinorKOrder(int[] rows, int[] cols)
        {
            if(rows.Length != cols.Length)
            {
                Console.WriteLine("\nНе рiвна кiлькiсть рядкiв i стовпцiв!");
                return 0;
            }

            double[,] minorMat = new double[rows.Length, cols.Length];
            double minor;

            for(int i = 0; i < rows.Length; i++)
            {
                for(int j = 0; j < cols.Length; j++)
                {
                    minorMat[i, j] = matrix[rows[i], cols[j]];
                }
            }

            for (int i = 0; i < minorMat.GetLength(0); i++)
            {
                for (int j = 0; j < minorMat.GetLength(1); j++)
                {
                    Console.Write(string.Format("{0} ", minorMat[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);

            minor = MatrixDeterminant(minorMat);
            return minor;
        }

        public double AdditionalMinorKOrder(int[] rows, int[] cols)
        {
            if (rows.Length != cols.Length)
            {
                Console.WriteLine("\nНе рiвна кiлькiсть рядкiв i стовпцiв!");
                return 0;
            }

            double[,] minorMat = new double[matrix.GetLength(0) - rows.Length, matrix.GetLength(0) - rows.Length];
            double minor;

            int m = 0, k, n = 0, r = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                k = 0;
                n = 0;
                if (i == rows[r])
                {
                    r++;
                    if (r >= rows.Length)
                        r -= 1;
                    continue;
                }

                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (j == cols[n])
                    {
                        n++;
                        if (n >= cols.Length)
                            n -= 1;
                        continue;
                    }
                    if (k >= minorMat.GetLength(1))
                        break;
                    minorMat[m, k++] = matrix[i, j];
                    
                }
                m++;
            }

            for (int i = 0; i < minorMat.GetLength(0); i++)
            {
                for (int j = 0; j < minorMat.GetLength(1); j++)
                {
                    Console.Write(string.Format("{0} ", minorMat[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);

            minor = MatrixDeterminant(minorMat);
            return minor;
        }

        static public double MinorIJ(Matrix matrix, int row, int col)
        {
            if(matrix.GetLength(0) != matrix.GetLength(1))
            {
                Console.WriteLine("\nМатриця не квадратна!");
                return 0;
            }
            int n = matrix.GetLength(0) - 1;
            double[,] minorMat = new double[n, n];
            int m = 0, k;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                k = 0;
                if (i == row)
                {
                    continue;
                }
                
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (j == col)
                    {
                        continue;
                    }
                    minorMat[m, k++] = matrix[i, j];
                }
                m++;
            }

            for (int i = 0; i < minorMat.GetLength(0); i++)
            {
                for (int j = 0; j < minorMat.GetLength(1); j++)
                {
                    Console.Write(string.Format("{0} ", minorMat[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);

            double minor = MatrixDeterminant(minorMat);
            return minor;
        }


        public static double MatrixDeterminant(double[,] matrix)
        {
            int n = int.Parse(Math.Sqrt(matrix.Length).ToString());
            int len = n - 1;
            int next;
            double p;
            double det = 1;

            double[,] arr = new double[matrix.GetLength(0), matrix.GetLength(1)];
            for(int i = 0; i < arr.GetLength(0); i++)
            {
                for(int j = 0; j < arr.GetLength(0); j++)
                {
                    arr[i, j] = matrix[i, j];
                }
            }

            for (int k = 0; k < len; k++)
            {
                next = k + 1;
                for (int i = next; i < n; i++)
                {
                    p = arr[i, k] / arr[k, k];
                    for (int j = next; j < n; j++)
                        arr[i, j] = arr[i, j] - p * arr[k, j];
                }
            }
            for (int i = 0; i < n; i++)
                det = det * arr[i, i];
            return det;
        }

        public static Matrix AddMatrixes(Matrix firstMat, Matrix secondMat)
        {
            if (firstMat.GetLength(0) != secondMat.GetLength(0) || firstMat.GetLength(1) != secondMat.GetLength(1))
            {
                Console.WriteLine("\nМатриці різної розмірності!");
                return null;
            }

            Matrix result = new Matrix(firstMat.GetLength(0), secondMat.GetLength(1));

            for (int i = 0; i < secondMat.GetLength(0); i++)
                for (int j = 0; j < secondMat.GetLength(1); j++)
                    result[i, j] = firstMat[i, j] + secondMat[i, j];

            return result;
        }

        public static Matrix SubstractMatrixes(Matrix firstMat, Matrix secondMat)
        {
            if (firstMat.GetLength(0) != secondMat.GetLength(0) || firstMat.GetLength(1) != secondMat.GetLength(1))
            {
                Console.WriteLine("\nМатриці різної розмірності!");
                return null;
            }

            Matrix result = new Matrix(firstMat.GetLength(0), secondMat.GetLength(1));

            for (int i = 0; i < secondMat.GetLength(0); i++)
                for (int j = 0; j < secondMat.GetLength(1); j++)
                    result[i, j] = firstMat[i, j] - secondMat[i, j];

            return result;
        }

        public static Matrix MultiplyMatrixes(Matrix firstMat, Matrix secondMat)
        {
            Matrix result = null;

            if (firstMat.GetLength(1) == secondMat.GetLength(0))
            {
                result = new Matrix(firstMat.GetLength(0), secondMat.GetLength(1));
                for (int i = 0; i < result.GetLength(0); i++)
                {
                    for (int j = 0; j < result.GetLength(1); j++)
                    {
                        result[i, j] = 0;
                        for (int n = 0; n < firstMat.GetLength(1); n++) 
                            result[i, j] = result[i, j] + firstMat[i, n] * secondMat[n, j];
                    }
                }
            }
            else
            {
                Console.WriteLine("\nНе рiвна кiлькiсть колонок в першiй матрицi i кiлькiсть рядкiв у другiй!");
            }

            return result;
        }

    }
}
