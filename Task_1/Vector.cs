﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task_1
{
    class Vector : IEnumerable<int>
    {
        private int[] vector;
        public int LowerBound { get; private set; }
        public int UpperBound { get; private set; }
        public int Length { get; private set; }

        protected Vector()
        {

        }

        public Vector(int lowerBound, int upperBound)
        {
            try
            {
                Length = upperBound - lowerBound + 1;
                vector = new int[Length];
                UpperBound = upperBound;
                LowerBound = lowerBound;
            }
            catch (Exception)
            {
                Console.WriteLine("\nUpper bound is less than lower!");
            }
        }

        public int this[int i] {
            get
            {
                try
                {
                    if(i > UpperBound || i < LowerBound)
                    {
                        throw new IndexOutOfRangeException();
                    }
                    return vector[i - LowerBound];
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    if (i - LowerBound - 1 < LowerBound)
                    {
                        return vector[0];
                    }
                    else
                    {
                        return vector[UpperBound - LowerBound - 1];
                    }
                }
            }
            set
            {
                try
                {
                    if (i > UpperBound || i < LowerBound)
                    {
                        throw new IndexOutOfRangeException();
                    }
                    vector[i - LowerBound] = value;
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("\nIndexOutOfRangeException!");
                }
                catch(NullReferenceException)
                {
                    Console.WriteLine("\nIndexOutOfRangeException");
                }
            }

        }

        public static Vector operator +(Vector firstVec, Vector secondVec)
        {
            if (firstVec.LowerBound != secondVec.LowerBound || firstVec.UpperBound != secondVec.UpperBound)
            {
                Console.WriteLine("\nNot allowed to add vectors with different bounds!");
                return new Vector();
            }

            Vector sum = new Vector(secondVec.LowerBound, secondVec.UpperBound);

            for (int i = secondVec.LowerBound; i < secondVec.UpperBound; i++)
            {
                sum[i] = firstVec[i] + secondVec[i];
            }

            return sum;
        }

        public static Vector operator -(Vector firstVec, Vector secondVec)
        {
            if (firstVec.LowerBound != secondVec.LowerBound || firstVec.UpperBound != secondVec.UpperBound)
            {
                Console.WriteLine("\nNot allowed to substract vectors with different bounds!");
                return new Vector();
            }

            Vector difference = new Vector(secondVec.LowerBound, secondVec.UpperBound);

            for (int i = secondVec.LowerBound; i < secondVec.UpperBound; i++)
            {
                difference[i] = firstVec[i] - secondVec[i];
            }

            return difference;
        }

        public static Vector operator *(Vector vec, int number)
        {
            Vector multVector = new Vector(vec.LowerBound, vec.UpperBound);

            for (int i = vec.LowerBound; i < vec.UpperBound; i++)
            {
                multVector[i] = vec[i] * number;
            }

            return multVector;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Vector);         
        }

        public bool Equals(Vector vec)
        {
            if(Object.ReferenceEquals(null, vec))
            {
                return false;
            }

            if ( vec.UpperBound - vec.LowerBound != this.UpperBound - this.LowerBound )
            {
                return false;
            }

            for (int i = LowerBound; i < vec.UpperBound; i++)
            {
                if (vec[i] != this[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                if(vector == null)
                {
                    return 0;
                }

                int hash = 17;
                foreach(var item in vector)
                {
                    hash = hash * 31 + item.GetHashCode();
                }
                return hash;
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            for(int i = 0; i < vector.Length; i++)
            {
                yield return vector[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public static bool operator ==(Vector firstVec, Vector secondVec)
        {
            if(Object.ReferenceEquals(firstVec, null))
            {
                return false;
            }
            return firstVec.Equals(secondVec);
        }

        public static bool operator !=(Vector firstVec, Vector secondVec)
        {
            if (Object.ReferenceEquals(firstVec, null))
            {
                return false;
            }
            return !firstVec.Equals(secondVec);
        }
    }

}
