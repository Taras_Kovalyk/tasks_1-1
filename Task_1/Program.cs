﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Program
    {
        static void PrintVector(Vector vec)
        {
            Console.WriteLine("\n");
            foreach(var item in vec)
            {
                Console.WriteLine("{0} ", item);
            }
        }

        static void Main(string[] args)
        {
            Vector vec = new Vector(8, 10);
            Vector vec2 = new Vector(8, 10);

            vec[8] = 9;
            vec[9] = 3;
            Console.WriteLine("\nВектор 1: ");
            PrintVector(vec);

            vec2[8] = 9;
            vec2[9] = 3;
            Console.WriteLine("\nВектор 2: ");
            PrintVector(vec2);

            bool ka = vec == vec2;

            Console.WriteLine("\nПорiвняння векторiв: {0}", ka);

            Vector sum = vec + vec2;
            Console.WriteLine("\nСума векторiв: ");
            PrintVector(sum);

            Vector difference = vec - vec2;
            Console.WriteLine("\nРiзниця векторiв: ");
            PrintVector(difference);

            Vector mult = vec * 4;
            Console.WriteLine("\nМноження вектора на 4: ");
            PrintVector(mult);

            Vector vec3 = new Vector(7, 18);
            Console.WriteLine("\nДодавання векторiв з рiзними iнтервалами iндексiв: ");
            Vector sum2 = vec + vec3;

            Console.WriteLine("\nДоступ до елемента за межами масиву: ");
            vec[4] = 9;

        }
    }
}
