﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Polynomials
    {
        int[] coefficients;
        public int Power { get; private set; }

        protected Polynomials()
        {

        }

        public Polynomials(int[] values)
        {
            coefficients = values;
            Power = coefficients.Length - 1;
        }

        public Polynomials(int numOfCoefs)
        {
            coefficients = new int[numOfCoefs];
            Power = numOfCoefs - 1;
        }

        public void AddPolynoms()
        {
            for(int i = 0; i < coefficients.Length; i++)
            {
                coefficients[i] = int.Parse(Console.ReadLine());
            }
        }

        public void ShowOnScreen()
        {
            Console.WriteLine(this.ToString());
        }

        public int FindFunction(int x)
        {
            int result = 1;
            int exp = Power;
            int func = 0;
            int i = 1;
            
            while (exp > 0)
            {
                result = result * x;
                func += result * coefficients[i];
                i++;
                exp--;
            }
            func += coefficients[0];
            return func;
        }

        public override string ToString()
        {
            string str = null;

            for (int i = coefficients.Length - 1; i >= 0; i--)
            {
                str += coefficients[i].ToString() + " ";
            }

            return str;
        }

        public static Polynomials operator +(Polynomials p1, Polynomials p2)
        {
            int lessLength = p1.Power < p2.Power ? p1.Power + 1 : p2.Power + 1;
            int[] biggerPolynom = p1.Power > p2.Power ? p1.coefficients : p2.coefficients;
            int biggerLength = biggerPolynom.Length;
            int[] result = new int[biggerLength];

            for(int i = 0; i < lessLength; i++)
            {
                result[i] = p1.coefficients[i] + p2.coefficients[i];
            }
            for (int i = lessLength; i < biggerLength; i++)
            {
                result[i] = biggerPolynom[i];
            }
            return new Polynomials(result);
        }

        public static Polynomials operator -(Polynomials p1, Polynomials p2)
        {
            int lessLength = p1.Power < p2.Power ? p1.Power + 1 : p2.Power + 1;
            int[] biggerPolynom = p1.Power > p2.Power ? p1.coefficients : p2.coefficients;
            int biggerLength = biggerPolynom.Length;
            int[] result = new int[biggerLength];

            for (int i = 0; i < lessLength; i++)
            {
                result[i] = p1.coefficients[i] - p2.coefficients[i];
            }
            for (int i = lessLength; i < biggerLength; i++)
            {
                result[i] = biggerPolynom[i];
            }
            return new Polynomials(result);
        }

        public static Polynomials operator *(Polynomials p1, Polynomials p2)
        {
            int resultLength = p1.coefficients.Length + p2.coefficients.Length - 1;
            int[] result = new int[resultLength];

            for (int i = 0; i < p1.coefficients.Length; i++)
            {
                for (int j = 0; j < p2.coefficients.Length; j++)
                {
                    result[i + j] += p1.coefficients[i] * p2.coefficients[j];
                }
            }

            return new Polynomials(result);
        }

    }
}

