﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[]
            {
                4, 7, 8
            };
            Polynomials p = new Polynomials(arr);

            int[] arr2 = new int[]
            {
                4, 7, 3
            };
            Polynomials p2 = new Polynomials(arr2);

            int x = 3, x2 = 2;
            
            Console.WriteLine("\nКоефiцiєнти многочлена №1: ");
            p.ShowOnScreen();
            Console.WriteLine("\nКоефiцiєнти многочлена №2: ");
            p2.ShowOnScreen();

            Polynomials sum = p + p2;
            Console.WriteLine("\nКоефiцiєнти многочлена-суми: ");
            sum.ShowOnScreen();

            Polynomials difference = p2 - p;
            Console.WriteLine("\nКоефiцiєнти многочлена-рiзницi: ");
            difference.ShowOnScreen();

            Polynomials mult = p2 * p;
            Console.WriteLine("\nКоефiцiєнти многочлена-добутку: ");
            mult.ShowOnScreen();

            int func = p.FindFunction(x);
            int func2 = p2.FindFunction(x2);
            Console.WriteLine("\nЗначення многочлена №1 для змiнної х = {0}: {1}", x, func.ToString());
            Console.WriteLine("\nЗначення многочлена №2 для змiнної х = {0}: {1}", x2, func2.ToString());
        }
    }
}
